package tp5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BinarySearchTreeTestAdd {

	BinarySearchTree<Integer, String> bst;
	
	@Before
	public void init(){
		bst = new BinarySearchTree<>();
		
	}
	
	@Test
	public void testAdd() {
		assertTrue(bst.add(22, "22"));
		assertTrue(bst.add(9, "9"));
		assertTrue(bst.add(41, "41"));
		assertTrue(bst.add(39, "39"));
		assertTrue(bst.add(27, "27"));
		assertTrue(bst.add(6, "6"));
		assertTrue(bst.add(11, "11"));
		assertTrue(bst.add(45, "45"));
		assertTrue(bst.add(59, "59"));
		assertTrue(bst.add(18, "18"));
		assertTrue(bst.add(36, "36"));
	}
	

}
