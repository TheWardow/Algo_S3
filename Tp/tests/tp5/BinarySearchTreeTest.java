package tp5;

import static org.junit.Assert.*;

import java.util.AbstractMap;

import org.junit.Before;
import org.junit.Test;

public class BinarySearchTreeTest {

	BinarySearchTree<Integer, String> bst;
	
	@Before
	public void init(){
		bst = new BinarySearchTree<>();
		bst.add(22, "22");
		bst.add(9, "9");
		bst.add(41, "41");
		bst.add(39, "39");
		bst.add(27, "27");
		bst.add(6, "6");
		bst.add(11, "11");
		bst.add(45, "45");
		bst.add(59, "59");
		bst.add(18, "18");
		bst.add(36, "36");
	}
	
	@Test
	public void testMin() {
		assertEquals("6",bst.min());
	}
	
	@Test
	public void testMax() {
		assertEquals("59",bst.max());
	}
	@Test
	public void testDeleteMin() {
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(6,"6"),bst.deleteMin());
		assertEquals("9",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(9,"9"),bst.deleteMin());
		assertEquals("11",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(11,"11"),bst.deleteMin());
		assertEquals("18",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(18,"18"),bst.deleteMin());
		assertEquals("22",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(22,"22"),bst.deleteMin());
		assertEquals("27",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(27,"27"),bst.deleteMin());
		assertEquals("36",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(36,"36"),bst.deleteMin());
		assertEquals("39",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(39,"39"),bst.deleteMin());
		assertEquals("41",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(41,"41"),bst.deleteMin());
		assertEquals("45",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(45,"45"),bst.deleteMin());
		assertEquals("59",bst.min());
		assertEquals(new AbstractMap.SimpleEntry<Integer,String>(59,"59"),bst.deleteMin());
		assertEquals(null,bst.min());
	}
	
	@Test
	public void testDelete() {
		bst.delete(45);
		assertEquals("[6, 9, 11, 18, 22, 27, 36, 39, 41, 59]", bst.toString());
		bst.delete(9);
		assertEquals("[6, 11, 18, 22, 27, 36, 39, 41, 59]", bst.toString());
		bst.delete(22);
		assertEquals("[6, 11, 18, 27, 36, 39, 41, 59]", bst.toString());
		bst.delete(39);
		assertEquals("[6, 11, 18, 27, 36, 41, 59]", bst.toString());
		bst.delete(27);
		assertEquals("[6, 11, 18, 36, 41, 59]", bst.toString());
		bst.delete(36);
		assertEquals("[6, 11, 18, 41, 59]", bst.toString());
		bst.delete(11);
		assertEquals("[6, 18, 41, 59]", bst.toString());
		bst.delete(59);
		assertEquals("[6, 18, 41]", bst.toString());
		bst.delete(18);
		assertEquals("[6, 41]", bst.toString());
		bst.delete(41);
		assertEquals("[6]", bst.toString());
		bst.delete(6);
		assertEquals("[]", bst.toString());
	}

}
