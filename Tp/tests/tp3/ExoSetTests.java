package tp3;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import tp4.ExoSet;

public class ExoSetTests {

	Set<Integer> s1, s2,s3,res, inters2s3, unions2s3;

	
	@Before
	public void init(){
		s1 = new HashSet<>();
		s1.addAll(Arrays.asList(new Integer[]{0,1,2,3,4,5,6,7,8,9}));
		s2 = new HashSet<>();
		s2.addAll(Arrays.asList(new Integer[]{3,4,5,6}));
		s3 = new HashSet<>();
		s3.addAll(Arrays.asList(new Integer[]{0,1,2,3,4}));
		
		inters2s3 = new HashSet<>();
		inters2s3.addAll(Arrays.asList(new Integer[]{3,4}));
		unions2s3= new HashSet<>();
		unions2s3.addAll(Arrays.asList(new Integer[]{0,1,2,3,4,5,6}));
	}

	@Test
	public void testInter(){
		res = ExoSet.inter(s2, s3);
		assertTrue(res.equals(inters2s3));
	}

	@Test
	public void testUnion(){
		res = ExoSet.union(s2,s3);
		assertTrue(res.equals(unions2s3));
	}

	@Test
	public void testIsIn(){
		assertTrue(ExoSet.isIn(s3, s1));
		assertFalse(ExoSet.isIn(s1, s3));
	}

}
