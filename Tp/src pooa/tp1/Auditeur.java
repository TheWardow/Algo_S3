package tp1;

import java.util.List;

public class Auditeur extends Observer {
	Tchat gui;
	
	public Auditeur(Tchat gui) {
		this.gui = gui;
	}
	@Override
	public void update(List<Object> l) {
		gui.textArea.setText(gui.textArea.getText() + "\n" + (String) l.get(0));
	}

}
