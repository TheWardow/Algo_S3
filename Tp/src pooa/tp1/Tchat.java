package tp1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Tchat extends JFrame {

	Observer observer;
	RadioTchat rt;
	JTextArea textArea;

	public Tchat(RadioTchat rt) {
		observer = new Auditeur(this);
		this.rt = rt;

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				rt.subject.detach(observer);
				e.getWindow().dispose();
				
			}
		});
		
		
		this.setTitle("Auditeur");
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(5,5));

		textArea = new JTextArea();		
		textArea.setEditable(false);
		JScrollPane textAreaScroll = new JScrollPane(textArea);
		textAreaScroll.setPreferredSize(new Dimension(300, 400));
		panel.add(textAreaScroll, BorderLayout.CENTER);

		
		JTextField input = new JTextField();
		input.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//textArea.setText(textArea.getText() + "\n" + input.getText());
				rt.textArea.append("\n" + input.getText());
				input.setText("");
				
				rt.subject.notifyObservers();
			}
		});
		panel.add(input, BorderLayout.SOUTH);

		this.getContentPane().add(panel);
		this.pack();
		this.setVisible(true);
	}

	public Observer getObserver(){
		return observer;
	}

}
