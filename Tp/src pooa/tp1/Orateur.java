package tp1;

import java.util.ArrayList;
import java.util.List;

public class Orateur extends Subject {
	RadioTchat gui;
	
	public Orateur(RadioTchat gui) {
		this.gui = gui;
	}
	
	@Override
	public List<Object> getArgs() {
		List<Object> l = new ArrayList<>();
		l.add(gui.getText());
		return l;
	}
	
	
}
