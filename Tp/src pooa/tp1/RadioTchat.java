package tp1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class RadioTchat extends JFrame{
	
	Subject subject;
	JTextArea textArea;
	RadioTchat rt = this;

	public RadioTchat(){
		subject = new Orateur(this);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Radio Tchat");
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(5,5));

		JButton newUserBtn = new JButton("Nouvel Auditeur");
		newUserBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				subject.attach(new Tchat(rt).getObserver());
			}
		});
		panel.add(newUserBtn, BorderLayout.NORTH);

		textArea = new JTextArea();		
		textArea.setEditable(false);
		JScrollPane textAreaScroll = new JScrollPane(textArea);
		textAreaScroll.setPreferredSize(new Dimension(300, 400));
		panel.add(textAreaScroll, BorderLayout.CENTER);

		JTextField input = new JTextField();
		input.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textArea.setText(textArea.getText() + "\n" + input.getText());
				input.setText("");
				subject.notifyObservers();
			}
		});
		panel.add(input, BorderLayout.SOUTH);

		this.getContentPane().add(panel);
		this.pack();
		this.setVisible(true);
	}

	public Object getText() {
		String[] split = textArea.getText().split("\n");
		return split[split.length-1];
	}
}
