package tp1;

import java.util.List;

public abstract class Observer {
	
	abstract public void update(List<Object> l);
}
