package tp2;

import java.util.Observable;

public class RGBModel extends Observable{

	private int r=0,g=0,b=0;

	public int getR() {return r;}

	public int getG() {return g;}

	public int getB() {return b;}

	public String getHex(){
		return Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b);
	}

	public void setR(int r) {
		this.r = r;
		setChanged();
		notifyObservers();
	}

	public void setG(int g) {
		this.g = g;
		setChanged();
		notifyObservers();
	}

	public void setB(int b) {
		this.b = b;
		setChanged();
		notifyObservers();
	}

	public void setHex(String hex){
		try{
			r=Integer.parseInt(hex.substring(0, 2),16);
			g=Integer.parseInt(hex.substring(2, 4),16);
			b=Integer.parseInt(hex.substring(4, 6),16);
		}catch(Exception e){
			e.printStackTrace(System.out);
		}

		setChanged();
		notifyObservers();
	}



}
