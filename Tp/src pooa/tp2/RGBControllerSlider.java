package tp2;

public class RGBControllerSlider {
	private RGBModel model ;
	private RGBVueSlider view;

	public RGBControllerSlider(RGBModel model) {
		this.model = model;
	}
	
	public void setR(int r){
		model.setR(r);
	}
	
	public void setG(int g){
		model.setR(g);
	}
	
	public void setB(int b){
		model.setR(b);
	}
	
	public void addView(RGBVueSlider view) {
		this.view = view ;
	}
}
