package tp2;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class RGBVueColor implements Observer{

	protected RGBModel model;
	private JPanel colorPanel;
	
	public RGBVueColor(RGBModel model) {
		colorPanel = new JPanel();
		colorPanel.setPreferredSize(new Dimension(100,100));
		this.model = model;
		model.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		colorPanel.setBackground(new Color(model.getR(), model.getG(), model.getB()));
	}

	public JPanel getPanel(){
		return colorPanel;
	}

}
