package tp2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class TemperatureVueThermometre implements Observer{
	protected TemperatureModel model ;
	protected TemperatureControllerThermometre controller ;
	private JFrame temperatureJFrame ;
	private JSlider slider;

	TemperatureVueThermometre(String label, TemperatureModel model, TemperatureControllerThermometre tempcontrolT, int posX, int posY ) {
		this.model = model ;
		this.controller = tempcontrolT ;
		this.slider = new JSlider();
		slider.setOrientation(JSlider.VERTICAL);
		slider.setMaximum(100);
		slider.setMinimum(-20);
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				controller.fixeDegresC(slider.getValue());
			}
		});
		temperatureJFrame = new JFrame (label);
		temperatureJFrame.add (slider, BorderLayout . CENTER );
		temperatureJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		model.addObserver(this); // Connexion entre la vue et le modele
		temperatureJFrame.setSize (100 ,300);
		temperatureJFrame.setLocation ( posX , posY );
		temperatureJFrame.setVisible ( true );
	}

	public void setSlider (int n) {
		slider.setValue(n);
	}

	public void enableWarningColor () {
		temperatureJFrame.setBackground(Color.RED);
	}

	public void disableWarningColor () {
		temperatureJFrame.setBackground(Color.WHITE);
	}

	public int getSliderValue() {
		return slider.getValue();
	}

	public void addChangeListener ( ChangeListener a ){ slider.addChangeListener(a);}

	protected TemperatureModel model(){
		return model ;
	}

	@Override
	public void update(Observable o, Object arg) {
		slider.setValue((int) model.getC());
		
	}

}
