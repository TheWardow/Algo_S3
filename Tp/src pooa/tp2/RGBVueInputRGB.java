package tp2;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RGBVueInputRGB implements Observer{

	protected RGBModel model;
	private RGBControllerInputRGB controller;
	private JTextField rInput, gInput, bInput;
	private JPanel rgbPanel;

	public RGBVueInputRGB(RGBModel model,RGBControllerInputRGB controller) {
		this.model = model;
		this.controller = controller;
		model.addObserver(this);
		
		rInput = new JTextField();
		gInput = new JTextField();
		bInput = new JTextField();
		rInput.setPreferredSize(new Dimension(50,25));
		gInput.setPreferredSize(new Dimension(50,25));
		bInput.setPreferredSize(new Dimension(50,25));
		
		rInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				controller.setR(rInput.getText());
			}
		});
		gInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				controller.setG(gInput.getText());
			}
		});
		bInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				controller.setB(bInput.getText());
			}
		});
		
		rgbPanel = new JPanel();
		rgbPanel.setLayout(new BoxLayout(rgbPanel, BoxLayout.Y_AXIS));
		rgbPanel.add(rInput);
		rgbPanel.add(gInput);
		rgbPanel.add(bInput);
	}

	@Override
	public void update(Observable o, Object arg) {
		rInput.setText(""+model.getR());
		gInput.setText(""+model.getG());
		bInput.setText(""+model.getB());
	}

	public JPanel getPanel(){
		return rgbPanel;
	}
}
