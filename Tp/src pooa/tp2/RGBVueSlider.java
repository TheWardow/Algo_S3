package tp2;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class RGBVueSlider implements Observer{

	protected RGBModel model;
	private RGBControllerSlider controller;
	private JSlider rSlider, gSlider, bSlider;
	private JPanel sliderPanel;

	public RGBVueSlider(RGBModel model,RGBControllerSlider controller) {
		this.model = model;
		this.controller = controller;
		model.addObserver(this);

		rSlider = new JSlider();
		gSlider = new JSlider();
		bSlider = new JSlider();
		rSlider.setMaximum(255);
		gSlider.setMaximum(255);
		bSlider.setMaximum(255);
		rSlider.setMinimum(0);
		gSlider.setMinimum(0);
		bSlider.setMinimum(0);

		rSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if(rSlider.getValueIsAdjusting())
					controller.setR(rSlider.getValue());
			}
		});
		gSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if(gSlider.getValueIsAdjusting())
					controller.setG(gSlider.getValue());
			}
		});
		bSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if(bSlider.getValueIsAdjusting())
					controller.setB(bSlider.getValue());
			}
		});

		sliderPanel = new JPanel();
		sliderPanel.setLayout(new BoxLayout(sliderPanel, BoxLayout.Y_AXIS));
		sliderPanel.add(rSlider);
		sliderPanel.add(gSlider);
		sliderPanel.add(bSlider);
	}

	@Override
	public void update(Observable o, Object arg) {
		rSlider.setValue(model.getR());
		gSlider.setValue(model.getG());
		bSlider.setValue(model.getB());
	}

	public JPanel getPanel(){
		return sliderPanel;
	}
}
