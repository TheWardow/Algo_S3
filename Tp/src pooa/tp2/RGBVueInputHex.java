package tp2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class RGBVueInputHex implements Observer{

	protected RGBModel model;
	private RGBControllerInputHex controller;
	private JTextField hexInput;
	private JPanel hexPanel;

	public RGBVueInputHex(RGBModel model,RGBControllerInputHex controller) {
		this.model = model;
		this.controller = controller;
		model.addObserver(this);
		controller.addView(this);
		
		hexInput = new JTextField();
		hexInput.setPreferredSize(new Dimension(100,25));
		hexInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				controller.setHex(hexInput.getText());
			}
		});
		hexPanel = new JPanel();
		hexPanel.add(hexInput);
	}

	@Override
	public void update(Observable o, Object arg) {
		hexInput.setText(model.getHex());
	}

	public JPanel getPanel(){
		return hexPanel;
	}
}
