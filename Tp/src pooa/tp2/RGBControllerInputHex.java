package tp2;

public class RGBControllerInputHex {
	private RGBModel model ;
	private RGBVueInputHex view;

	public RGBControllerInputHex(RGBModel model) {
		this.model = model;
	}
	
	public void setHex(String hex){
		model.setHex(hex);
	}
	
	public void addView(RGBVueInputHex view) {
		this.view = view ;
	}
}
