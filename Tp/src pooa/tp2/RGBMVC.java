package tp2;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class RGBMVC {

	public RGBMVC(){
		RGBModel model = new RGBModel();
		RGBControllerSlider controllerSlider = new RGBControllerSlider(model);
		RGBControllerInputRGB controllerRgb = new RGBControllerInputRGB(model);
		RGBControllerInputHex controllerHex = new RGBControllerInputHex(model);
		RGBVueSlider vs = new RGBVueSlider(model, controllerSlider);
		RGBVueInputRGB vi = new RGBVueInputRGB(model, controllerRgb);
		RGBVueInputHex vh = new RGBVueInputHex(model, controllerHex);
		RGBVueColor vc = new RGBVueColor(model);
		controllerSlider.addView(vs);
		controllerRgb.addView(vi);
		controllerHex.addView(vh);
		
		JFrame frame = new JFrame("RGBSelector");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.add(vs.getPanel());
		panel.add(vi.getPanel());
		panel.add(vh.getPanel());
		panel.add(vc.getPanel());
		frame.pack();
		frame.setVisible(true);
		
		
	}
	
	public static void main ( String args []) {
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application’s GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new RGBMVC();
			}
		});
	}
}
