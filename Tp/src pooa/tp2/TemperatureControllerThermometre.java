package tp2;

public class TemperatureControllerThermometre {
	private TemperatureModel model ;
	private TemperatureVueThermometre view = null;

	public TemperatureControllerThermometre(TemperatureModel m){
		model = m ;
	}

	public void fixeDegresC(double tempC){
		model.setC(tempC);
		control();
	}

	public void control() {
		if(view != null) {
			if(model.getC() > 40.0) {
				view.enableWarningColor();
			} else {
				view.disableWarningColor();
			}
		}
	}

	public void addView(TemperatureVueThermometre view) {
		this.view = view ;
	}

}
