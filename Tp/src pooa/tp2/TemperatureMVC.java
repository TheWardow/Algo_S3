package tp2;

public class TemperatureMVC {
	public TemperatureMVC () {
		TemperatureModel tempmod = new TemperatureModel();
		TemperatureController tempcontrolC = new TemperatureController(tempmod);
		TemperatureController tempcontrolF = new TemperatureController(tempmod);
		TemperatureControllerThermometre tempcontrolT = new TemperatureControllerThermometre(tempmod);
		TemperatureVueCelsuis tvc = new TemperatureVueCelsuis (tempmod, tempcontrolC, 100, 200);
		TemperatureVueFarenheit tvf = new TemperatureVueFarenheit(tempmod, tempcontrolF, 100, 350);
		TemperatureVueThermometre tvt = new TemperatureVueThermometre("Celsius", tempmod, tempcontrolT, 450, 150);
		tempcontrolC.addView(tvc);
		tempcontrolF.addView(tvf);
		tempcontrolT.addView(tvt);
	}
	public static void main ( String args []) {
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application’s GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new TemperatureMVC ();
			}
		});
	}
}