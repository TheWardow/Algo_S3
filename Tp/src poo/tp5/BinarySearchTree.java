package tp5;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

public class BinarySearchTree <K extends Comparable<K>, V>{
	private K key;
	private V value;
	private Comparator<K> comp;
	private BinarySearchTree<K,V> left, right;

	//CONSTRUCTORS
	public BinarySearchTree() {
		key = null;
		value = null;
		comp = null;
		left = null;
		right = null;
	}
	public BinarySearchTree(K key, V value){
		this.key = key;
		this.value = value;
		comp = null;
		left = null;
		right = null;
	}
	public BinarySearchTree(K key, V value, Comparator<K> comp){
		this.key = key;
		this.value = value;
		this.comp = comp;
		left = null;
		right = null;
	}
	private void initTree(BinarySearchTree<K, V> t) {
		key = t.key;
		value = t.value;
		left = t.left;
		right = t.right;
		comp = t.comp;

	}

	//MIN MAX
	public BinarySearchTree<K,V> bstMin(){
		if(this.isEmpty())
			return null;
		if (left.isEmpty())
			return this;
		return left.bstMin();
	}
	public BinarySearchTree<K, V> bstMax(){
		if(this.isEmpty())
			return null;
		if (right.isEmpty())
			return this;
		return right.bstMax();
	}
	public V min(){
		//System.out.println("Liste : " + this);
		BinarySearchTree<K, V> bst = bstMin();
		if(bst != null)
			return bst.value;
		
		return null;
	}
	public V max (){
		BinarySearchTree<K, V> bst = bstMax();
		if(bst != null)
			return bst.value;
		
		return null;
	}

	private boolean isEmpty() {
		return key == null && left == null && right == null;
	}

	private int compare(K k1, K k2){
		if (comp != null)
			return comp.compare(k1, k2);
		return ((Comparable<K>)k1).compareTo(k2);
	}

	//OPERATIONS
	public boolean add(K key, V value){
		if (this.isEmpty()){
			this.key = key;
			this.value = value;
			left = new BinarySearchTree<>();
			right = new BinarySearchTree<>();
			return true;
		}
		if(compare(key, this.key) > 0){
			return right.add(key, value);
		}
		if(compare(key,this.key) < 0){
			return left.add(key, value);
		}
		return false;
	}

	public AbstractMap.SimpleEntry<K, V> deleteMin(){
		if(isEmpty())
			return null;

		if(left.isEmpty())
		{
			AbstractMap.SimpleEntry<K, V> r = new AbstractMap.SimpleEntry<K,V>(this.key, this.value) ;
			initTree(right);
			return r;
		}
		else
		{
			return left.deleteMin();
		}

	}
	private BinarySearchTree<K, V> bstDeleteMin(){
		if(isEmpty())
			return null;

		if(left.isEmpty())
		{
			BinarySearchTree<K, V> r  = new BinarySearchTree<K, V>(this.key, this.value);
			initTree(right);
			return r;
		}
		else
		{
			return left.bstDeleteMin();
		}
	}

	public void delete(K key){
		if(isEmpty())
			return;

		if (compare(key,this.key) < 0)
			left.delete(key);
		else if (compare (key, this.key) > 0)
			right.delete(key);
		else
			if(left.isEmpty())
				initTree(right);
			else if (right.isEmpty())
				initTree(left);
			else{
				BinarySearchTree<K, V> oldLeft = left;
				BinarySearchTree<K, V> oldRight = right;
				initTree(right.bstDeleteMin());
				left = oldLeft;
				right = oldRight;
			}
	}

	public V get(K key){
		if(!isEmpty()){
			if (compare(key,this.key) < 0)
				left.get(key);
			else if (compare (key, this.key) > 0)
				right.get(key);
			else
				return this.value;
		}

		return null;
	}


	protected List<V> infixe(){
		if(isEmpty())
			return new ArrayList<V>();
		List<V> res = new ArrayList<>();
		if(left != null)
			res = left.infixe();

		res.add(value);
		if(right != null)
			res.addAll(right.infixe());		
		return res;
	}
	protected  LinkedList<BinarySearchTree<K, V>> infixeBST(){
		if(isEmpty())
			return new LinkedList<BinarySearchTree<K, V>>();
		LinkedList<BinarySearchTree<K, V>> res = left.infixeBST();
		res.add(this);
		res.addAll(right.infixeBST());		
		return res;
	}

	public String toString(){
		List<V> list = infixe();
		return list.toString();

	}

	public BSTIterator<K,V> iterator(){
		return new BSTIterator<K, V>(this);
	}



	public static void main(String args[]){
		BinarySearchTree<Integer, String> bst = new BinarySearchTree<>();
		bst.add(22, "22");
		bst.add(9, "9");
		bst.add(41, "41");
		bst.add(39, "39");
		bst.add(27, "27");
		bst.add(6, "6");
		bst.add(11, "11");
		bst.add(45, "45");
		bst.add(59, "59");
		bst.add(18, "18");
		bst.add(36, "36");

		BSTIterator<Integer, String> it = (BSTIterator<Integer, String>) bst.iterator();
		while(it.hasNext())
			System.out.println(it.next());


	}
}

class BSTIterator<K extends Comparable<K>,V> implements Iterator<K>{

	BinarySearchTree<K,V> bst;
	Stack<K> s = new Stack<>();

	public BSTIterator(BinarySearchTree<K, V> tree) {
		bst = tree;
		//empiler les neuds de la racine jusqu'au min
		
		
		//si fils droit : empiler le fils droit puis tout les elements jusqu'au min du fils droit
		//sinon dépiler
	}

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public K next() {
		return null;
	}

	@Override
	public void remove() {

	}
}