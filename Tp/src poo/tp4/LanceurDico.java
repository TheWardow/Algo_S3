package tp4;

public class LanceurDico {

	public static void main(String[] args) {
		Dictionnary<String, String> dico = new Dictionnary<>();
		dico.put("ordinateur", "computer");
		dico.put("un", "a");
		dico.put("très", "very");
		dico.put("gros", "big");
		
		String s = "un très gros ordinateur";
		String s2 = "a very big computer";

		String rep = "";
		for(String sp : s.split(" ")){
			rep += dico.getFromPrimary(sp) + " "; 
		}
		System.out.println(rep);
		
		rep = "";
		for(String sp : s2.split(" ")){
			rep += dico.getFromSecondary(sp) + " "; 
		}
		System.out.println(rep);
		
		System.out.println(dico);
	}

}
