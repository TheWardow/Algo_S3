package tp4;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class ExoSet {

	public static void main(String[] args) {
		Set<Integer> ens1, ens2;
		ens1 = new HashSet<>();
		ens2 = new HashSet<>();

		for (int i = 0;i<10; i++){
			ens1.add(new Random().nextInt(100));
			ens2.add(new Random().nextInt(100));
		}

		afficher(ens1);
		afficher(ens2);

	}
	public static Set<Integer> inter(Set<Integer> set1, Set<Integer> set2){
		Set<Integer> s3 = new HashSet<>();
		for(Integer i : set1){
			if (set2.contains(i))
				s3.add(i);
		}
		return s3;

	}
	public static Set<Integer> union(Set<Integer> set1, Set<Integer> set2){
		Set<Integer> s3 = new HashSet<>();
		for(Integer i : set1){
			s3.add(i);
		}
		for(Integer i : set2){
			s3.add(i);
		}
		return s3;
	}

	public static boolean isIn(Set<Integer> set1, Set<Integer> set2){
		for(Integer i : set1){
			if(! set2.contains(i))
				return false;
		}
		return true;
	}

	public static void afficher(Set ens){
		String s = "[";
		s += ens.size() + "]{";
		for(Object i : ens){
			s += i + ",";
		}	
		s+="}";
		System.out.println(s);
	}

}
