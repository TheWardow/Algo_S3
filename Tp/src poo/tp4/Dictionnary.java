package tp4;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Dictionnary<K1, K2> implements BidirectionnalMap<K1, K2> {

	private Map<K1,K2> primary = new HashMap<>();
	private Map<K2,K1> secondary = new HashMap<>();

	@Override
	public K2 getFromPrimary(K1 k) {
		return primary.get(k);
	}

	@Override
	public K1 getFromSecondary(K2 k) {
		return secondary.get(k);
	}

	@Override
	public void put(K1 k1, K2 k2) {
		primary.put(k1, k2);
		secondary.put(k2, k1);
	}

	@Override
	public boolean isEmpty() {
		return primary.isEmpty();
	}

	@Override
	public void clear() {
		primary.clear();
		secondary.clear();
	}

	@Override
	public void removeFromPrimary(K1 k1) {
		secondary.remove(primary.remove(k1));
	}

	public String toString(){
		String s = "";
		Set<Entry<K1, K2>> entrySet = primary.entrySet();
		for(Entry<K1, K2> e : entrySet){
			s += e.toString()+"\n";
		}
		return s;
		
	}



}
