/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

/**
 *
 * @author jedrzejw
 */
public class Counter {

    private int comp, perm;

    public void incComp() {
        comp++;
    }

    public void incComp(int n) {
        comp += n;
    }

    public void incPerm() {
        perm++;
    }

    public void incPerm(int n) {
        perm += n;
    }
    
    public void reset(){
        perm=0;
        comp=0;
    }

    public String toString(){
        return "(comparaisons:" + comp + " ,permutations:" + perm + ")"; 
    }
}
