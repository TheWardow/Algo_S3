package tp1;

import java.util.Arrays;

/**
 * SDD Seance TP 1 :
 *
 * @author <a href="mailto:Frederic.Guyomarch@univ-lille1.fr">FrÃ©dÃ©ric
 * Guyomarch</a>
 * IUT-A, Universite de Lille, Sciences et TEchnologies
 */
public class Sort {

    public static int[] generateRdmIntArray(int n, int min, int max) {
        int[] tab = new int[n];
        for (int i = 0; i < n; ++i) {
            tab[i] = (int) (min + Math.random() * (max - min));
        }
        return tab;
    }

    public static void insertSort(int[] tab) {
        for (int i = 0; i < tab.length; i++) {
            int x = tab[i];
            int j = i;
            while (j > 0 && tab[j - 1] > x) {
                tab[j] = tab[j - 1];
                j = j - 1;
            }
            tab[j] = x;
        }
    }
    public static void insertSort(int[] tab,Counter c) {
        for (int i = 0; i < tab.length; i++) {
            c.incComp();
            int x = tab[i];
            int j = i;
            while (j > 0 && tab[j - 1] > x) {
                c.incComp();
                c.incPerm();
                tab[j] = tab[j - 1];
                j = j - 1;
            }
            tab[j] = x;
        }
    }

    public static void selectSort(int[] tab) {
        for (int b = 0; b < tab.length; b++) {
            swap(tab, b, minIdx(tab, b, tab.length));
        }
    }
    public static void selectSort(int[] tab,Counter c) {
        for (int b = 0; b < tab.length; b++) {
            c.incComp();
            swap(tab, b, minIdx(tab, b, tab.length));
            c.incPerm();
        }
    }

    private static int minIdx(int[] tab, int iBeg, int iEnd) {
        int idx = iBeg;
        for (int i = iBeg + 1; i < iEnd; ++i) {
            if (tab[i] < tab[idx]) {
                idx = i;
            }
        }

        return idx;
    }

    public static void printArray(int[] tab) {
        System.out.println(Arrays.toString(tab));
    }

    public static void swap(int[] tab, int idx, int idx2) {
        int tmp = tab[idx];
        tab[idx] = tab[idx2];
        tab[idx2] = tmp;
    }

    public static void bubbleSort(int[] tab) {
        boolean fini = false;
        while (!fini) {
            fini = true;
            for (int i = 0; i < tab.length - 1; ++i) {
                if (tab[i] > tab[i + 1]) {
                    swap(tab, i, i + 1);
                    fini = false;
                }
            }
        }
    }
    public static void bubbleSort(int[] tab,Counter c) {
        boolean fini = false;
        while (!fini) {
            c.incComp();
            fini = true;
            for (int i = 0; i < tab.length - 1; ++i) {
                c.incComp();
                if (tab[i] > tab[i + 1]) {
                    c.incComp();
                    swap(tab, i, i + 1);
                    c.incPerm();
                    fini = false;
                }
            }
        }
    }

}
