/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

/**
 *
 * @author jedrzejw
 */
public class main {

    public static void main(String[] args) {
        final int[] UNSORTED = {22, 11, 8, 17, 47, 98, 25, 34, 69, 52};
        Counter c = new Counter();
        int[] unSorted;

        c.reset();
        unSorted = UNSORTED.clone();
        Sort.bubbleSort(unSorted, c);
        System.out.println("Bubble : " + c);

        c.reset();
        unSorted = UNSORTED.clone();
        Sort.insertSort(unSorted, c);
        System.out.println("Insert : " + c);

        c.reset();
        unSorted = UNSORTED.clone();
        Sort.selectSort(unSorted, c);
        System.out.println("Select : " + c);
    }
}
