/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

/**
 *
 * @author jedrzejw
 */
public class MyList<E> {

    Node<E> first;
    Node<E> last;

    public MyList() {
        first = null;
        last = null;
    }

    public boolean add(E e) {
        Node<E> n = new Node(e, null);
        if (first == null) {
            first = n;
        } else {
            last.next = n;
        }
        last = n;
        return true;
    }

    @Override
    public String toString() {
        String s = "";
        if (first != null) {
            Node<E> c = first;
            s += c.value;
            while (c.next != null) {
                c = c.next;
                s += " " + c.value;
            }
            return s;
        }
        return "";
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        int cpt = 0;
        Node<E> c = first;
        while (c.next != null) {
            cpt++;
            c = c.next;
        }
        return cpt;
    }

    public void clear() {
        first = null;
        last = null;
    }

    public E get(int index) {
        if (index > this.size()) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> c = first;
        int cpt = 0;
        while (cpt < index) {
            cpt++;
        }
        return c.value;
    }

    public int indexOf(Object o) {
        int cpt = 0;
        Node<E> c = first;
        while (c.next != null) {
            if (c.value.equals(o)) {
                return cpt;
            }
            cpt++;
            c = c.next;
        }
        return -1;
    }

    public boolean contains(Object o) {
        Node<E> c = first;
        while (c.next != null) {
            if (c.value.equals(o)) {
                return true;
            }
            c = c.next;
        }
        return false;
    }

    public int lastIndexOf(Object o) {
        Node<E> c = first;
        int i = -1;
        int cpt = 0;
        while (c.next != null) {
            if (c.value.equals(o)) {
                i = cpt;
            }
            c = c.next;
            cpt++;
        }
        return i;
    }

    public void add(int index, E element) throws NoSuchMethodException {
        throw new NoSuchMethodException();
    }

    public E remove(int index) throws NoSuchMethodException {
        throw new NoSuchMethodException();
    }

    boolean remove(Object o) throws NoSuchMethodException {
        throw new NoSuchMethodException();
    }

    public class Node<E> {

        E value;
        Node<E> next;

        public Node(E val, Node<E> next) {
            value = val;
            this.next = next;
        }
    }
}
