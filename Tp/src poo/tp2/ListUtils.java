package tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class ListUtils {

    public static void main(String args[]) {
        List<Integer> l = genRdmIntList();
        affiche(l);
        afficheInverse(l);
        System.out.println(somme(l));
    }

    public static List<Integer> genRdmIntList() {
        List<Integer> l = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            l.add(new Random().nextInt(99) + 1);
        }
        return l;
    }

    public static void affiche(List<Integer> l) {
        Iterator<Integer> i = l.iterator();
        String s = "" + i.next();
        while (i.hasNext()) {
            s += " -> " + i.next();
        }
        System.out.println(s);
    }

    public static void afficheInverse(List<Integer> l) {
        ListIterator<Integer> i = l.listIterator(l.size());
        String s = "" + i.previous();
        while (i.hasPrevious()) {
            s += " -> " + i.previous();
        }
        System.out.println(s);
    }

    public static int somme(List<Integer> l) {
        int somme = 0;
        Iterator<Integer> i = l.iterator();
        while (i.hasNext()) {
            somme += i.next();
        }
        return somme;
    }

    public static int moyenne(List<Integer> l) {
        return somme(l) / l.size();
    }

    public static int max(List<Integer> l) {
        int max = 0;
        Iterator<Integer> i = l.iterator();
        while (i.hasNext()) {
            int tmp = i.next();
            if (max < tmp) {
                max = tmp;
            }
        }
        return max;
    }

    public static int mIN(List<Integer> l) {
        int min = Integer.MAX_VALUE;
        Iterator<Integer> i = l.iterator();
        while (i.hasNext()) {
            int tmp = i.next();
            if (min > tmp) {
                min = tmp;
            }
        }
        return min;
    }

    public static List<Integer> positions(List<Integer> l, int n) {
        Iterator<Integer> i = l.iterator();
        List<Integer> p = new ArrayList<Integer>();
        int cpt = 0;
        while (i.hasNext()) {
            if (i.next().equals(new Integer(n))) {
                p.add(cpt);
            }
            cpt++;
        }

        return p;

    }

    static List<Integer> paire(List<Integer> l) {
        Iterator<Integer> i = l.iterator();
        List<Integer> p = new ArrayList<Integer>();
        while (i.hasNext()) {
            int tmp = i.next();
            if (tmp % 2 == 0) {
                p.add(tmp);
            }
        }
        return p;
    }

    static boolean estTrie(List<Integer> l) {
        Iterator<Integer> i = l.iterator();
        int last = i.next();
        while (i.hasNext()) {
            int tmp = i.next();
            if (tmp < last) {
                return false;
            }
        }
        return true;
    }

    static List<Integer> trie(List<Integer> l) {
        Collections.sort(l);
        return l;
    }

}
