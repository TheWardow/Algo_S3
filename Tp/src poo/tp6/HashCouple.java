package tp6;

import java.util.Map;

public class HashCouple<K,V> implements Map.Entry<K,V>{

	private K key;
	private V val;
	
	public HashCouple(K key, V value) {
		this.key = key;
		val = value;
	}

	@Override
	public K getKey() {
		return key;
	}

	@Override
	public V getValue() {
		return val;
	}

	@Override
	public V setValue(V value) {
		V old = val;
		val = value;
		return old;
	}
	
	public int hashCode(){
		return key.hashCode();
	}
	
	public String toString(){
		return key + "," + val;
	}
}
