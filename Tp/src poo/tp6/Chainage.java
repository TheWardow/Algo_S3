package tp6;

import java.util.Iterator;
import java.util.LinkedList;


public class Chainage<K,V> implements HashTable<K,V>{
	public static final int DEFAULT_SIZE = 16;
	LinkedList<HashCouple<K, V>>[] l;

	@SuppressWarnings("unchecked")
	public Chainage() {
		l = (LinkedList<HashCouple<K, V>>[]) new LinkedList[DEFAULT_SIZE];
		for(int i = 0; i<l.length; i++){
			l[i]= new LinkedList<HashCouple<K, V>>();
		}
	}

	@Override
	public V put(K key, V value) {
		V old = null;
		for (HashCouple<K, V> couple : l[Math.abs(key.hashCode())%l.length])
			if(couple.getKey().equals(key)){
				old = couple.getValue();
				couple.setValue(value);
			}

		l[key.hashCode()%l.length].add(new HashCouple<K, V>(key, value));
		return old;
	}

	@Override
	public V get(K key) {
		V val = null;
		for (HashCouple<K, V> couple : l[Math.abs(key.hashCode())%l.length])
			if(couple.getKey().equals(key)){
				val = couple.getValue();
			}
		return val;
	}

	@Override
	public boolean remove(K key) {
		Iterator<HashCouple<K, V>> it = l[Math.abs(key.hashCode())%l.length].iterator();
		
		while(it.hasNext()){
			HashCouple<K, V> couple = it.next();
			if(couple.getKey().equals(key)){
				it.remove();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean contains(K key) {
		for (HashCouple<K, V> couple : l[Math.abs(key.hashCode())%l.length])
			if(couple.getKey().equals(key)){
				return true;
			}
		return false;
	}

	@Override
	public int size() {
		int i = 0;
		for(LinkedList<HashCouple<K, V>> ll : l)
			i += ll.size();
		return i;
	}
	
	public String toString(){
		String s = "Chainage[";
		for(LinkedList<HashCouple<K, V>> ll : l){
			for (HashCouple<K, V> c : ll){
				s += "{" + c.toString() + "}";
			}
		}
		return s + "]";
	}
}
