package tp6;

public class MainHashTable {
	public static void main (String args[]){
		Chainage<String, Integer> map = new Chainage<>();
		map.put("trois", 3);
		map.put("dix", 10);
		map.put("quarante-deux", 42);
		System.out.println(map);
		System.out.println("get dix : " + map.get("dix"));
		System.out.println("contains 42 ? " + map.contains("quarante-deux"));
		System.out.println("containt 70 ? " + map.contains("septante"));
		System.out.println("size : " + map.size());
		System.out.println("remove dix ? " + map.remove("dix"));
		System.out.println(map);
	}
}
