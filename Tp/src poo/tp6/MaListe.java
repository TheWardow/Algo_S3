package tp6;

public class MaListe<K,V> {

	HashCouple<K, V> couple;
	MaListe<K, V> next;
	
	public MaListe() {
		couple = null;
		next = null;
	}
	
	public void add(HashCouple<K, V> couple){
		if(this.couple == null)
			this.couple = couple;
		else{
			next = new MaListe<K,V>();
			next.add(couple);
		}
	}
	
	public HashCouple<K, V> remove(K key){
		if(this.couple.getKey().equals(key)){
			HashCouple<K, V> c = this.couple;
			
			return c;
		}
		else{
			next.remove(key);
		}
		return couple;
	}
	
}
