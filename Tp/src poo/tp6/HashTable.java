package tp6;

public interface HashTable<K,V> {

	public V put(K key, V value);
	public V get(K key);
	public boolean remove (K key);
	public boolean contains(K key);
	public int size();
}
