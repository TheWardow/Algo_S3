package tp3;

public class File {
	private Cellule[] f = new Cellule[1000];
	private int queue = 0;
	private int tete = 0;
	private int nbItems = 0;
	
	public boolean isEmpty(){
		return nbItems == 0;
	}

	public void offer(Cellule cellule) {
		f[queue++] = cellule;
		nbItems++;
		
	}

	public Cellule remove() {
		Cellule c =f[tete];
		f[tete++] = null;
		nbItems--;
		return c;
	}
}
