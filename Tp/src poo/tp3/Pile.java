package tp3;

public class Pile{
	private Cellule[] p = new Cellule[100];
	int last = -1;

	public void add(Cellule cellule) {
		p[++last] = cellule;
	}

	public boolean isEmpty() {
		return last == -1;
	}

	public Cellule pop() {
		Cellule c = p[last];
		p[last--] = null;
		return c;
	}

}
