package tp3;

public class Cellule {
	public int x,y;
	
	public Cellule(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public String toString(){
		return "(" + x + "," + y + ")"; 
	}
}
