package tp3;

import java.util.LinkedList;
import java.util.Stack;

public class Parcours {

	public static void main(String[] args) {
		Labyrinthe l = new Labyrinthe();

		//profondeur(l);
		largeur(l);

	}

	private static void largeur(Labyrinthe l) {
		//Stack<Cellule> p = new Stack<>();
		//Pile p = new Pile();
		LinkedList<Cellule> q = new LinkedList<>();
		//File q = new File();

		//p.add(new Cellule(0, 1));
		q.offer(new Cellule(0, 1));
		l.poserMarque(0, 1);

		//while(! p.isEmpty()){
		while(! q.isEmpty()){

			try {
				Thread.sleep(1) ;
			}
			catch(InterruptedException e){}

			//Cellule c = p.peek();
			Cellule c = q.getFirst();
			System.out.println("Explore : (" + c.x + "," + c.y +")");
			//Cellule c = q.remove();
			l.poserMarqueRetour(c.x, c.y);
			if (c.x == l.n()-1 && c.y == l.n()-2){
				System.out.println("Sortie trouv�e");
				//for(Cellule cell : p){ 
				for(Cellule cell : q){ 
					l.poserMarqueChemin(cell.x, cell.y);
				}
				return;
			}
			else{
				// teste pour ajout de l'un des voisins
				try{
					if(!l.estMur(c.x, c.y-1) && !l.estMarque(c.x, c.y-1)){
						//p.add(new Cellule(c.x, c.y-1));
						q.offer(new Cellule(c.x, c.y-1));
						l.poserMarque(c.x, c.y-1);
					}
					else if(!l.estMur(c.x+1, c.y) && !l.estMarque(c.x+1, c.y)){
						//p.add(new Cellule(c.x+1, c.y));
						q.offer(new Cellule(c.x+1, c.y));
						l.poserMarque(c.x+1, c.y);
					}
					else if(!l.estMur(c.x, c.y+1) && !l.estMarque(c.x, c.y+1)){
						//p.add(new Cellule(c.x, c.y+1));
						q.offer(new Cellule(c.x, c.y+1));
						l.poserMarque(c.x, c.y+1);
					}
					else if(!l.estMur(c.x-1, c.y) && !l.estMarque(c.x-1, c.y)){
						//p.add(new Cellule(c.x-1, c.y));
						q.offer(new Cellule(c.x-1, c.y));
						l.poserMarque(c.x-1, c.y);
					}
					// sinon p.pop();
					else {
						//p.pop();
						q.remove();
					}
				}catch(StringIndexOutOfBoundsException e){}


			}
		}
		System.out.println("Il n'y a pas de sortie !");

	}

	private static void profondeur(Labyrinthe l) {
		Stack<Cellule> p = new Stack<>();
		//Pile p = new Pile();
		//LinkedList<Cellule> q = new LinkedList<>();
		//File q = new File();

		p.add(new Cellule(0, 1));
		//q.offer(new Cellule(0, 1));
		l.poserMarque(0, 1);

		while(! p.isEmpty()){
			//while(! q.isEmpty()){

			try {
				Thread.sleep(1) ;
			}
			catch(InterruptedException e){}

			Cellule c = p.peek();
			System.out.println("Explore : (" + c.x + "," + c.y +")");
			//Cellule c = q.remove();
			l.poserMarqueRetour(c.x, c.y);
			if (c.x == l.n()-1 && c.y == l.n()-2){
				System.out.println("Sortie trouv�e");
				for(Cellule cell : p){ 
					l.poserMarqueChemin(cell.x, cell.y);
				}
				return;
			}
			else{
				// teste pour ajout de l'un des voisins
				if(!l.estMur(c.x, c.y-1) && !l.estMarque(c.x, c.y-1)){
					p.add(new Cellule(c.x, c.y-1));
					//q.offer(new Cellule(i, j));
					l.poserMarque(c.x, c.y-1);
				}
				else if(!l.estMur(c.x+1, c.y) && !l.estMarque(c.x+1, c.y)){
					p.add(new Cellule(c.x+1, c.y));
					//q.offer(new Cellule(i, j));
					l.poserMarque(c.x+1, c.y);
				}
				else if(!l.estMur(c.x, c.y+1) && !l.estMarque(c.x, c.y+1)){
					p.add(new Cellule(c.x, c.y+1));
					//q.offer(new Cellule(i, j));
					l.poserMarque(c.x, c.y+1);
				}
				else if(!l.estMur(c.x-1, c.y) && !l.estMarque(c.x-1, c.y)){
					p.add(new Cellule(c.x-1, c.y));
					//q.offer(new Cellule(i, j));
					l.poserMarque(c.x-1, c.y);
				}
				// sinon p.pop();
				else {
					p.pop();
				}


			}
		}
		System.out.println("Il n'y a pas de sortie !");

	}
}
